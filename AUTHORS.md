CREDITS
=======

See the file [AUTHORS.old](AUTHORS.old) for an historical list of authors. Their
contributions over the years have been unvaluable.

Current maintainer
------------------

AbiWord current maintainer is:

Hubert Figuière <hub@figuiere.net>

